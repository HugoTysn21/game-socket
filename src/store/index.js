import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router/index'

const {ipcRenderer} = require('electron')

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        socket: {
            // SEND MESSAGE FROM BROWSER TO THE SOCKET
            send: (msg, event = 'message-from-browser') => {
                ipcRenderer.send(event, msg)
            },
        },
        player: {isConnected: false, ready: false, cards: [], score: "0"},
        board: [],
        timeOutHandleReady: null,
        timeOutHandleInit: null,
        isOpenReady: false,
        preStartMessage: false,
        firstPreStartMessage: false,
        couldChooseLine: false,
        patientPlayer: false,
        countdown: 25,
        hasPlayedCard: false
    },
    mutations: {

        // mutations are sync

        SET_PLAYER_CONNECTED(state) {
            state.player.isConnected = true
            state.firstPreStartMessage = true
        },

        SET_READY_MESSAGE(state) {
            state.firstPreStartMessage = false
            state.preStartMessage = true
        },
        SET_READY_MODAL(state) {
            state.isOpenReady = true
        },
        SET_PLAYER_READY(state) {
            state.player.ready = true;
        },
        SET_BOARDS(state, data){
            state.board = data
            state.patientPlayer = false
            //console.log(state, data)
        },
        SET_CARDS_PLAYER(state, data) {
            if(state.player.cards.length > 0){
                state.player.cards = data
            } else {
                state.player.cards = data
                router.push({path: '/home'}).then(() => {})
            }
        },
        SET_LINE(state){
            state.couldChooseLine = true

        },
        SET_PATIENT_PLAYER(state){
            state.patientPlayer = true
        },
        SET_PLAYED_CARD(state){
            // play card -> action disabled
            state.hasPlayedCard = true
        },
        SET_PLAYER_SCORE(state, data){
            state.player.score = data
        }
    },
    actions: {
        // mutations are async or sync

        listenSocketMsg({state, commit}) {
            // listen double event, socket.js / store
            ipcRenderer.on('message-from-socket', (event, payload) => {

                console.log(payload)
                const obj = JSON.parse(payload)

                if (obj.event === 'START_TIMER') {
                    commit('SET_READY_MESSAGE')
                    state.timeOutHandleInit = obj.value;
                }

                // open modal Ready and init timer ready
                if (obj.event === 'ACK_INIT') {
                    commit('SET_READY_MODAL')
                    let interval = setInterval(()=>{
                        if (state.countdown === 0){
                            if (!state.player.ready){
                                ipcRenderer.send('DISCONNECT')
                                ipcRenderer.send('exit-app')
                            }
                            clearInterval(interval)
                        } else {
                            state.countdown--
                        }
                    }, 1000)
                }
                if (obj.event === 'BOARD') {
                    commit('SET_BOARDS', JSON.parse(obj.value))
                    state.socket.send('CARDS')
                }

                if (obj.event === 'CARDS') {
                    commit('SET_CARDS_PLAYER', JSON.parse(obj.value))
                    state.hasPlayedCard = false
                    state.socket.send('SCORE')
                }
                if (obj.event === 'SCORE') {
                    commit('SET_PLAYER_SCORE', JSON.parse(obj.value))
                }
                if (obj.event === 'CHOOSE') {
                    commit('SET_LINE')
                }
                if (obj.event === 'PATIENT') {
                    commit('SET_PATIENT_PLAYER')
                }
            })
        },
    }
})
