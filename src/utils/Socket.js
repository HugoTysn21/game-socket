const net = require('net')
const { ipcMain } = require('electron')

export default class Socket {

    constructor(host, port, win) {
        this.host = host;
        this.port = port;
        this.win = win;
        this.client = null

        this.connectToSocket();
    }

    connectToSocket() {
        this.client = net.connect({ host: this.host, port: this.port })
    }
    listenMsg() {
        this.listenBrowserMsg()
        this.listenSocketMsg()
    }

    // message du browser renderer -> ipc main -> socketTCP
    // processus main non valide dans le client pour cause d'isolation du process electron:node
    listenBrowserMsg() {
        ipcMain.on('message-from-browser', (event, arg) => {
            this.sendMsgToSocket(arg)
        })
    }

    // message du socket -> browser
    listenSocketMsg() {
        this.client.on('data', (data) => {
            this.sendMsgToBrowser(data)
        })
    }

    sendMsgToSocket(msg) {
        console.log(msg)
        this.client.write(msg)
    }

    sendMsgToBrowser(data) {
        this.win.webContents.send('message-from-socket', data.toString())
    }
}